<?php include_once 'connectDB.php'; ?>
<?php
header('Content-Type: application/json');

if (!empty($_POST))
{
  //Obteniendo las fechas de los input
  $inicio = $_POST["fInicial"];
  $final = $_POST["fFinal"];
  $tipo = $_POST["tipo"];

  //Dando formato a las fechas
  $inicio = str_replace('-','',$inicio);
  $final = str_replace('-','',$final);


  /*************** TRABAJANDO CON VENTAS ***************/
  $stmt1 = $db->prepare("{call Ventas_Fechas(@fDesde=?, @fHasta=?, @cTipo=?)}");
  $stmt1->bindParam(1, $inicio, PDO::PARAM_STR);
  $stmt1->bindParam(2, $final, PDO::PARAM_STR);
  $stmt1->bindParam(3, $tipo, PDO::PARAM_STR);
  $stmt1->execute();

  $ventas = array();
  $stmt1->nextRowset();
  while($result = $stmt1->fetch(PDO::FETCH_ASSOC))
	{
		array_push($ventas, array('label' => $result['fecha'], 'data' => $result['total']));
	}

	sort($ventas);
	/******************** FIN VENTAS ********************/
  
  
  /**************** TRABAJANDO CON PROMOCIONES ****************/
  $stmt2 = $db->prepare("{call Promociones_Fechas(@fDesde=?, @fHasta=?)}");
  $stmt2->bindParam(1, $inicio, PDO::PARAM_STR);
  $stmt2->bindParam(2, $final, PDO::PARAM_STR);
  $stmt2->execute();

  $promociones = array();
  while($result = $stmt2->fetch(PDO::FETCH_ASSOC))
	{
		array_push($promociones, array('label' => $result['Promocion'], 'data' => $result['cantidad']));
	}

	sort($promociones);
	/********************* FIN PROMOCIONES *********************/


	/*************** TRABAJANDO CON FORMA DE VENTA ***************/
  $stmt3 = $db->prepare("{call Cantidad_Forma_Venta(@fDesde=?, @fHasta=?)}");
  $stmt3->bindParam(1, $inicio, PDO::PARAM_STR);
  $stmt3->bindParam(2, $final, PDO::PARAM_STR);
  $stmt3->execute();

  $formaVenta = array();
  $stmt3->nextRowset();
  $stmt3->nextRowset();
  while($result = $stmt3->fetch(PDO::FETCH_ASSOC))
	{
		array_push($formaVenta, array('label' => $result['tipo'], 'data' => $result['cant']));
	}

	sort($formaVenta);
	/****************** FIN CON FORMA DE VENTA ******************/

	/*************** TRABAJANDO CON PROMEDIO DE VENTAS ***************/
  $stmt4 = $db->prepare("{call Porcentage_Ticket(@fDesde=?, @fHasta=?)}");
  $stmt4->bindParam(1, $inicio, PDO::PARAM_STR);
  $stmt4->bindParam(2, $final, PDO::PARAM_STR);
  $stmt4->execute();
  $resultPVentas = $stmt4->fetch(PDO::FETCH_ASSOC);
  
  $promedioVentas = $resultPVentas['Promedio'];
	/******************** FIN PROMEDIO DE VENTAS ********************/

	/******************** TRABAJANDO CON MERMA ********************/
  $stmt5 = $db->prepare("{call Cantidad_Merma(@fDesde=?, @fHasta=?)}");
  $stmt5->bindParam(1, $inicio, PDO::PARAM_STR);
  $stmt5->bindParam(2, $final, PDO::PARAM_STR);
  $stmt5->execute();
  $resultMerma = $stmt5->fetch(PDO::FETCH_ASSOC);
  
  $merma = $resultMerma['merma'];
	/************************* FIN MERMA *************************/

	/****************** TRABAJANDO CON VENTA DIARIA ***************/
  $stmt6 = $db->prepare("{call Mayor_Venta_Dia(@fDesde=?, @fHasta=?)}");
  $stmt6->bindParam(1, $inicio, PDO::PARAM_STR);
  $stmt6->bindParam(2, $final, PDO::PARAM_STR);
  $stmt6->execute();
  
  $ventaDiaria = array();
  $stmt6->nextRowset();
  while($result = $stmt6->fetch(PDO::FETCH_ASSOC))
	{
		array_push($ventaDiaria, array('label' => $result['fecha'], 'data' => $result['Ventas']));
	}

	sort($ventaDiaria);
	/********************** FIN VENTA DIARIA **********************/
	

	
	//Asignando datos al arreglo de respuesta
  $result = array(
  	'promociones' => $promociones,
  	'ventas' => $ventas,
  	'formaVenta' => $formaVenta,
  	'promedioVentas' => $promedioVentas,
  	'merma' => $merma,
  	'ventaDiaria' => $ventaDiaria,
  );

  //Devolviendo datos
	echo json_encode($result);
}

  

