<?php include_once 'master.php'; ?>
<?php include_once 'connectDB.php'; ?>

<?php
  //Si se realizó un POST...
  if (!empty($_POST))
  {
    //Obteniendo las fechas de los input
    $inicio = $_POST["fInicial"];
    $final = $_POST["fFinal"];

    //Dando formato a las fechas
    $inicio = str_replace('-','',$inicio);
    $final = str_replace('-','',$final);

    //Ejecutando procedimiento almacenado y pasando las variables
    $stmt = $db->prepare("{call Ventas_Fechas(@fDesde=?, @fHasta=?)}");
    $stmt->bindParam(1, $inicio, PDO::PARAM_STR);
    $stmt->bindParam(2, $final, PDO::PARAM_STR);
    $stmt->execute();
  }
?>

<div class="main-content">
  <div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center"
    style="min-height: 400px; background-image: url(../assets/img/theme/profile-cover.jpg); background-size: cover; background-position: center top;">
    <!-- Mask -->
    <span class="mask bg-gradient-default opacity-8"></span>
    <!-- Header container -->
    <div class="container-fluid d-flex align-items-center">
      <div class="row">
        <div class="col-lg-12 col-md-12">
          <h1 class="display-2 text-white">Reporte de ventas</h1>
        </div>
      </div>
    </div>
  </div>
  <!-- Page content -->
  <div class="container-fluid mt--8">
    <div class="row">
      <div class="col-xl-12 order-xl-1">
        <div class="card bg-secondary shadow">
          <div class="card-header bg-white border-0">
            <div class="row align-items-center">
              <div class="col-8">
                <h3 class="mb-0">Ingresa un rango de fecha </h3>
              </div>
            </div>
          </div>
          <form method="POST" action="reporteVentas.php">
            <div class="card-body">
              <div class="pl-lg-4">
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="form-control-label" for="input-username">Fecha Inicial</label>
                      <input type="date" class="form-control form-control-alternative" placeholder="Fecha Incial" name="fInicial">
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="form-control-label" for="input-email">Fecha Final</label>
                      <input type="date" class="form-control form-control-alternative" placeholder="Fecha Final" name="fFinal">
                    </div>
                  </div>
                </div>
                <button class="btn btn-primary" type="submit">Buscar</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <p></p>
  <div class="row">
    <div class="col">
      <div class="card shadow">
        <div class="card-header border-0">
          <h3 class="mb-0">Resultado</h3>
        </div>
        <div class="table-responsive">
          <table class="table align-items-center table-flush">
            <thead class="thead-light">
              <tr>
                <th scope="col">Total</th>
                <th scope="col">Fecha</th>
              </tr>
            </thead>
            <tbody>
              <?php
                if(isset($stmt))
                {
                  $stmt->nextRowset();
                  while($result = $stmt->fetch(PDO::FETCH_ASSOC))
                  {
                    echo "<tr>".
                            "<td>".
                              $result['total'].
                            "</td>".
                            "<td>".
                              $result['fecha'].
                            "</td>".
                          "</tr>";
                  }
                }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<?php include_once 'footer.php'; ?>

<script defer type="text/javascript">
  $('#tabReporteVentas').addClass('active');
</script>