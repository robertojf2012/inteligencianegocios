<?php include_once 'master.php'; ?>

<div class="main-content">
	<div class="header pb-12 pt-5 pt-lg-8 d-flex align-items-center">
		<!-- Page content -->
		<div class="container-fluid mt--8">
		  <div class="row">
		    <div class="col-xl-12">
		      <div class="card bg-secondary shadow">
		          <div class="card-body">
		            <div class="pl-lg-12">
		              <div class="row">
		                <div class="col-lg-4">
		                  <div class="form-group">
		                    <label class="form-control-label" for="input-username">Fecha Inicial</label>
		                    <input id="fInicial" type="date" class="form-control datepicker form-control-alternative" placeholder="Fecha Incial" name="fInicial">
		                  </div>
		                </div>
		                <div class="col-lg-4">
		                  <div class="form-group">
		                    <label class="form-control-label" for="input-email">Fecha Final</label>
		                    <input id="fFinal" type="date" class="form-control form-control-alternative" placeholder="Fecha Final" name="fFinal">
		                  </div>
		                </div>
		                <div class="col-lg-4">
		                  <div class="form-group">
		                    <label class="form-control-label" for="input-email">Tipo para ventas</label>
				                <select id="tipoVentas" class="form-control form-control-alternative">
												  <option value="Días">Días</option>
												  <option value="Meses">Meses</option>
												</select>
		                  </div>
		                </div>
		              </div>
		              <button class="btn btn-primary" type="button" onclick="actualizarGraficas();">Buscar</button>
		            </div>
		          </div>
		      </div>
		    </div>
		  </div>
		</div>
	</div>

	<br>

	<div class="container-fluid mt--12">
	  
	  <div class="row">
	  	<div class="col-xl-6 col-lg-6">
	      <div class="card card-stats mb-4 mb-xl-0">
	        <div class="card-body">
	          <div class="row">
	            <div class="col">
	              <h5 class="card-title text-uppercase text-muted mb-0">Promedio de ventas, 										(Valor de ticket promedio)</h5>
	              <span id="promedioVentas" class="h2 font-weight-bold mb-0"></span>
	            </div>
	            <div class="col-auto">
	              <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
	                <i class="ni ni-money-coins"></i>
	              </div>
	            </div>
	          </div>
	        </div>
	      </div>
	    </div>
	    <div class="col-xl-6 col-lg-6">
	      <div class="card card-stats mb-4 mb-xl-0">
	        <div class="card-body">
	          <div class="row">
	            <div class="col">
	              <h5 class="card-title text-uppercase text-muted mb-0">Merma</h5>
	              <span id="merma" class="h2 font-weight-bold mb-0"></span>
	            </div>
	            <div class="col-auto">
	              <div class="icon icon-shape bg-warning text-white rounded-circle shadow">
	                <i class="ni ni-box-2"></i>
	              </div>
	            </div>
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	  	
	  <br>

	  <div class="row">
	  	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-4">
	      <div class="card bg-secondary shadow">
	      		<div class="card-header border-0">
              <h2 class="mb-0">Ventas</h2>
            </div>
	          <div class="card-body">
	            <div class="pl-4">
	              <div class="row">
	               <canvas id="ventasChart" width="400" height="400"></canvas>
	              </div>
	            </div>
	          </div>
	      </div>
	    </div>
	    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-4 col-centered">
	      <div class="card bg-secondary shadow">
	      		<div class="card-header border-0">
              <h2 class="mb-0">Promociones</h2>
            </div>
	          <div class="card-body">
	            <div class="pl-4">
	              <div class="row">
	               <canvas id="promocionesChart" width="400" height="400"></canvas>
	              </div>
	            </div>
	          </div>
	      </div>
	    </div>
	    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-4">
	      <div class="card bg-secondary shadow">
	      		<div class="card-header border-0">
              <h2 class="mb-0">Forma de venta</h2>
            </div>
	          <div class="card-body">
	            <div class="pl-4">
	              <div class="row">
	               <canvas id="formaVentaChart" width="400" height="400"></canvas>
	              </div>
	            </div>
	          </div>
	      </div>
	    </div>
	  </div>
	  
	  <br>

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-4">
		    <div class="card bg-secondary shadow">
		  		<div class="card-header border-0">
		        <h2 class="mb-0">Mayor venta del dia</h2>
		      </div>
		      <div class="card-body">
		        <div class="pl-4">
		          <div class="row">
		           <canvas id="ventaDiariaChart" width="400" height="400"></canvas>
		          </div>
		        </div>
		      </div>
		    </div>
		  </div>
		</div>
	  

	</div>

</div>

<?php include_once 'footer.php'; ?>
<script src="./assets/Chart.js"></script>
<script defer type="text/javascript">
	$('#tabInicio').addClass('active');
</script>



<script type="text/javascript">
	var chartVentas;
	var chartPromociones;
	var chartFormaVenta;
	var chartVentaDiaria;

	function actualizarGraficas(){

		var fInicial = $("#fInicial").val();
		var fFinal = $("#fFinal").val();
		var tipoVentas = $("#tipoVentas").val();

		$.ajax({
        cache: false,
        type: "POST",
        url: 'graficas.php',
        data: { 'fInicial': fInicial, 'fFinal' : fFinal, 'tipo': tipoVentas },
        success: function (data) {
        	console.log(data);
        	graficaVentas(data.ventas);
        	graficaPromociones(data.promociones);
        	graficaFormaVenta(data.formaVenta);
        	graficaVentaDiaria(data.ventaDiaria);

        	$('#promedioVentas').text(data.promedioVentas);
        	$('#merma').text(data.merma);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(thrownError);
        }
    });
	}


	function graficaVentas(dataVentas){

		var labels = [];
		var data = [];

		for (var i = 0; i < dataVentas.length; i++) {
			labels.push(dataVentas[i]['label']);
			data.push(dataVentas[i]['data']);
		}

		//Validando si la grafica ya existe.. para eliminarla
		if(chartVentas){
			chartVentas.destroy();
		}

		//Generando grafica
		var ctx = document.getElementById('ventasChart');
		chartVentas = new Chart(ctx, {
	    type: 'line',
	    data: {
	      labels: labels,
	      datasets: [{
	        label: 'Ventas',
	        data: data,
	        backgroundColor: 'rgba(94, 114, 228, 0.4)',
	        borderColor: 'rgba(94, 114, 228, 0.4)',
	        borderWidth: 1
	      }]
	    }
		});
	}

	function graficaPromociones(dataPromociones){

		var labels = [];
		var data = [];
		var colors = [];

		for (var i = 0; i < dataPromociones.length; i++) {
			labels.push(dataPromociones[i]['label']);
			data.push(dataPromociones[i]['data']);
			colors.push(getRandomColor());
		}

		//Validando si la grafica ya existe.. para eliminarla
		if(chartPromociones){
			chartPromociones.destroy();
		}

		//Generando grafica
		var ctx = document.getElementById('promocionesChart');
		chartPromociones = new Chart(ctx, {
		    type: 'bar',
		    data: {
		        labels: labels,
		        datasets: [{
		            label: 'Ventas',
		            data: data,
		            backgroundColor: colors,
		            borderWidth: 1
		        }]
		    },
		    options: {
		        scales: {
		            yAxes: [{
		                ticks: {
		                    beginAtZero: true
		                }
		            }]
		        }
		    }
		});
	}

	function graficaFormaVenta(dataForma){

		var labels = [];
		var data = [];
		var colors = [];

		for (var i = 0; i < dataForma.length; i++) {
			labels.push(dataForma[i]['label']);
			data.push(dataForma[i]['data']);
			colors.push(getRandomColor());
		}

		//Validando si la grafica ya existe.. para eliminarla
		if(chartFormaVenta){
			chartFormaVenta.destroy();
		}

		//Generando grafica
		var ctx = document.getElementById('formaVentaChart');
		chartFormaVenta = new Chart(ctx, {
	    type: 'pie',
	    data: {
	      labels: labels,
	      datasets: [{
	        label: 'Cantidad',
	        data: data,
	        backgroundColor: colors,
	        borderWidth: 1
	      }]
	    },
	    options: {
	      scales: {
	        yAxes: [{
	          ticks: {
	              beginAtZero: true
	          }
	        }]
	      }
	    }
		});
	}

	function graficaVentaDiaria(dataVentaDiaria){

		var labels = [];
		var data = [];
		var colors = [];

		for (var i = 0; i < dataVentaDiaria.length; i++) {
			labels.push(dataVentaDiaria[i]['label']);
			data.push(dataVentaDiaria[i]['data']);
			colors.push(getRandomColor());
		}

		//Validando si la grafica ya existe.. para eliminarla
		if(chartVentaDiaria){
			chartVentaDiaria.destroy();
		}

		//Generando grafica
		var ctx = document.getElementById('ventaDiariaChart');
		chartVentaDiaria = new Chart(ctx, {
		    type: 'bar',
		    data: {
		        labels: labels,
		        datasets: [{
		            label: 'Ventas',
		            data: data,
		            backgroundColor: colors,
		            borderWidth: 1
		        }]
		    },
		    options: {
		        scales: {
		            yAxes: [{
		                ticks: {
		                    beginAtZero: true
		                }
		            }]
		        }
		    }
		});
	}

	function getRandomColor(){
		var rgb = [];
		for(var i = 0; i < 3; i++)
		    rgb.push(Math.floor(Math.random() * 255));

		return 'rgb('+ rgb.join(',') +',0.4)';
	}

</script>


