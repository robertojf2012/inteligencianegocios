<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>
      Inteligencia de negocios
    </title>
    <!-- Favicon -->
    <link href="./assets/img/brand/favicon.png" rel="icon" type="image/png">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <!-- Icons -->
    <link href="./assets/js/plugins/nucleo/css/nucleo.css" rel="stylesheet" />
    <link href="./assets/js/plugins/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link href="./assets/css/argon-dashboard.css?v=1.1.0" rel="stylesheet" />
  </head>
  <body class="">
    <nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
      <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Brand -->
        <a class="navbar-brand pt-0" href="./index.html">
          <img src="./assets/img/brand/blue.png" class="navbar-brand-img" alt="...">
        </a>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Navigation -->
          <ul class="navbar-nav">
            <li class="nav-item">
              <a id="tabInicio" class="nav-link" href="index.php">
                <i class="ni ni-tv-2 text-primary"></i> Inicio
              </a>
            </li>
            <li class="nav-item">
              <a id="tabReporteVentas" class="nav-link " href="reporteVentas.php">
                <i class="ni ni-planet text-blue"></i> Reporte de Ventas
              </a>
            </li>
            <li class="nav-item">
              <a id="tabPromedioVentas" class="nav-link " href="promedioVentas.php">
                <i class="ni ni-pin-3 text-orange"></i> Promedio de ventas
              </a>
            </li>
            <li class="nav-item">
              <a id="tabPromociones" class="nav-link " href="promociones.php">
                <i class="ni ni-single-02 text-yellow"></i> Promociones
              </a>
            </li>
            <li class="nav-item">
              <a id="tabMerma" class="nav-link " href="merma.php">
                <i class="ni ni-bullet-list-67 text-red"></i> Merma
              </a>
            </li>
            <li class="nav-item">
              <a id="tabVentaDiaria" class="nav-link" href="ventaDiaria.php">
                <i class="ni ni-key-25 text-info"></i> Venta Diaria
              </a>
            </li>
            <li class="nav-item">
              <a id="tabVentaForma" class="nav-link" href="formaVenta.php">
                <i class="ni ni-circle-08 text-pink"></i> Cantidad de forma de venta
              </a>
            </li>
          </ul>
          <!-- Divider -->
          <hr class="my-3">
        </div>
      </div>
    </nav>